use aws_sdk_dynamodb::{Client, types::AttributeValue};
use lambda_runtime::{run, service_fn, LambdaEvent, Error};
use serde_json::Value;
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use rand::Rng;

#[derive(Debug, Serialize, Deserialize)]
struct RandomNumberRecord {
    entry_id: String,
    number: i32,
}

// Main function
async fn handler(_event: LambdaEvent<Value>) -> Result<Value, Error> { // Prefix with underscore to indicate unused
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);
    let rng = rand::thread_rng().gen_range(1..101);
    let entry_id = Uuid::new_v4().to_string();

    let record = RandomNumberRecord {
        entry_id: entry_id.clone(), // Clone `entry_id` for later use
        number: rng,
    };

    client.put_item()
        .table_name("randomnumber")
        .item("entry_id", AttributeValue::S(entry_id)) // Use cloned `entry_id`
        .item("number", AttributeValue::N(record.number.to_string()))
        .send()
        .await?;

    Ok(serde_json::json!({
        "message": "Random number stored successfully",
        "entry_id": record.entry_id, // `entry_id` is now valid here as we cloned it before
        "number": record.number
    }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(handler);
    run(func).await
}
