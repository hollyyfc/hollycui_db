[![pipeline status](https://gitlab.com/hollyyfc/hollycui_db/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_db/-/commits/main)

# 📊 Holly's Lambda Function with Database 

## Description

This [Rust](https://www.rust-lang.org/) project is a serverless [AWS Lambda](https://aws.amazon.com/lambda/) function designed to **generate and store a random number** in an [AWS DynamoDB](https://aws.amazon.com/dynamodb/) table each time when invoked, showcasing a practical application of cloud-native technologies for data handling and storage. 

To send an invoke request, the user can either configure locally and then invoke remotely:
```
cargo lambda invoke --remote mini5 --data-ascii '{}' 
```
or invoke directly through the API URL: 
```
curl -X POST https://oowq9ud7mc.execute-api.us-east-1.amazonaws.com/finish \
  -H 'content-type: application/json' \
  -d '{}'
```
Since the function does not need any input when invoked, the above data ascii field should be left blank. After the invocation, the Lambda function *mini5* will randomly generate a number using crate `rand` and record the number value in a DynamoDB table *randomnumber* using crate `aws-sdk-dynamodb`. 

## Demo

- AWS Lambda function: *mini5*

![lambda](https://gitlab.com/hollyyfc/hollycui_db/-/wikis/uploads/38b36f21ba56eb8203ac0812944ec88c/lambda.png)

- API Gateway: 

![api](https://gitlab.com/hollyyfc/hollycui_db/-/wikis/uploads/2523e779d4b7a5f48abad16c32509d9f/api.png)

- Successful remote invoke: 

![local_invoke](https://gitlab.com/hollyyfc/hollycui_db/-/wikis/uploads/25e58ac24a4e533716373392a8ffe73f/local_invoke.png)

- AWS DynamoDB table: *randomnumber* 

![db_items](https://gitlab.com/hollyyfc/hollycui_db/-/wikis/uploads/1bbf916b1e2b9734e4ced5cd79b1e5b1/db_items.png)


## Steps Walkthrough

- **Build Cargo Project**
    - `cargo lambda new <YOUR-PROJECT-NAME>` in desired directory
    - Build Rust Lambda functions in `src/main.rs` 
    - Add `aws-sdk-dynamodb`, `lambda_runtime`, `lambda_http`, `aws-config` and relevant dependencies to `Cargo.toml` by `cargo add <dependency-name>` (This will attach the correponding versions automatically!)
    - `cargo lambda watch` for an initial function-build test
- **AWS Configuration**
    - AWS IAM -> Users -> Create user -> Attach policies: `AWSLambda_FullAccess` and `IAMFullAccess`
    - Open the new user page: Create access key -> Save `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` (preferrably the same as your previous lambda functions' region)
    - Store AWS config details in a `.env` file in the root of the project
    - Export the above details by `export AWS_ACCESS_KEY_ID=abcdefg` in a terminal
- **Connect to AWS**
    - `cargo lambda build --release`
    - `cargo lambda deploy`
    - AWS Lambda -> Check the function is there!
    - Click on the function -> Configuration -> Role name -> Attach policy: `AWSDynamoDBFullAccess`
    - AWS DynamoDB -> Create table -> Set a table name (must match with the one used in your `src/main.rs`, `randomnumber` in my case) and a partition key (i.e., primary key, `entry_id` in my case)
    - Test remote invoke: `cargo lambda invoke --remote <LAMBDA-FUNCTION-NAME>` ✅
- **Deploy with API**
    - AWS API Gateway -> Create a new API (REST API)
    - Inside the new API: Create resource
    - Inside the new resource: 
        - Create method
        - Method type: `ANY`
        - Integration type: `Lambda function`
        - Lambda proxy integration: off
        - Lambda function: choose the (Region, Function ARN)
    - Stages: Create stage -> Deploy -> Find invoke URL (mine: https://oowq9ud7mc.execute-api.us-east-1.amazonaws.com/finish)
    - Test API invoke: use the `curl` command above ✅



